# Cara memformat README
Readme gunanya untuk memudahkan orang lain agar paham dengan proyek apa yang kamu kerjakan di sini.

## Nama Proyek
Hal pertama yang dilihat orang.

## Deskripsi
Jelas, singkat, dan langsung ke intinya.

## Daftar Isi
Bersifat pilihan, gunanya untuk memudahkan navigasi dalam readme yang panjang.
```
[Headers](#headers)  
[Emphasis](#emphasis)  
...snip...    
<a name="headers"/>
## Headers
```
## Installasi
Menceritakan tentang bagaimana orang lain dapat menginstall apa yang kamu buat ini di komputer mereka. Sebaiknya diberi gambar bergerak untuk memudahkan mereka paham caranya.

## Penggunaan
Menjelaskan cara bagaimana menggunakan apa yang kamu buat ini, sebaiknya ditambahkan _screenshot_ dari apa yang telah kamu buat.

## Berkontribusi
Menjelaskan bagaimana caranya jika ada orang lain yang ingin ikut mengembangkan apa yang kamu buat. Biasanya ini berada di file yang terpisah dari.

## Kredit
Orang-orang yang terlibat dalam apa yang telah kamu kerjakan.

## Lisensi

Readme sebaiknya hanya berisi informasi yang perlukan agar orang lain dapat menggunakan dan mengembangkan apa yang telah kamu buat. Ya, mungkin itu saja