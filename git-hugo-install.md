# Push Hugo Web to Gitlab

```
# initialize new git repository
git init

# add /public directory to our .gitignore file
echo "/public" >> .gitignore

# commit and push code to master branch
git add .
git commit -m "Bismillah"
git remote add origin https://gitlab.com/n5/n5.gitlab.io.git
git push -u origin master
```

untuk memeriksa apakah pages sudah bisa diakses
```
https://gitlab.com/n5/n5.gitlab.io/pipelines
```
